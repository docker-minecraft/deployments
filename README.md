# Deployments

## Usage
```
$ apt update
$ apt install curl -y
$ curl -s https://dds.birkhoff.me/download/prepare > prepare
$ chmod +x prepare
$ ./prepare
```

Or: 

```
apt update; apt install curl -y; curl -s https://dds.birkhoff.me/download/prepare > prepare && chmod +x prepare && ./prepare
```