#!/bin/bash

####################################################
#                                                  #
#             Property of Birkhoff Lee             #
#          DO NOT REDISTRIBUTE THIS FILE           #
#                                                  #
#               https://birkhoff.me                #
#                                                  #
####################################################

# ---

pushd ~ &>/dev/null

bold=$(tput bold)
reset=$(tput sgr0)
prefix="$bold== "

# ---

log() {
    echo $prefix$1$reset
}

confirm_deploy() {
    read -r -p "${prefix}$1${reset}" response
    case "$response" in
        [yY][eE][sS]|[yY]) 
            true
            ;;
        [aA]) 
            log "Use Ctrl-P Ctrl-Q to escape. DO NOT use Ctrl-C. Use \"stop\" to stop the server. Attaching to $PAPER_CONTAINER_NAME ..."
            echo
            docker attach $PAPER_CONTAINER_NAME
            echo
            log "Exiting ... (run again to deploy the server)"
            exit 0
            ;;
        *)
            false
            ;;
    esac
}

# ---

if [ -z "$1" ]; then
    log "Please provide a valid server to deploy."
    log "For example: deploy_paper skywars1"
    exit 1
fi

SERVER_CFG_FILE_PATH="/usr/local/bin/cfg/paper/$1.cfg"
declare -a required_arguments=(
    "PAPER_CONTAINER_NAME"
    "PAPER_JAVA_LAUNCHFLAGS"
    "PAPER_DATA_PATH"
    "PAPER_DOCKER_ADDITIONAL_FLAGS"
    "PAPER_VERSION")

if [ ! -f "$SERVER_CFG_FILE_PATH" ]; then
    log "Couldn't find that server."
    exit 1
fi

for i in "${required_arguments[@]}"; do
    if ! grep -q "$i=" "$SERVER_CFG_FILE_PATH" ; then
        log "The configuration file of that server is corrupt, please contact me."
        exit 1
    fi
done

. $SERVER_CFG_FILE_PATH

IMAGE_NAME=$MC_DOCKER_REGISTRY_HOST/minecraft/paper:$PAPER_VERSION

# ---

if [ "$(docker ps -aq -f name=$PAPER_CONTAINER_NAME -f status=running)" ]; then
    docker update --restart=no $PAPER_CONTAINER_NAME &>/dev/null
    CONFIRM_DEPLOY_MESSAGE="Deploying PaperSpigot $PAPER_VERSION to $1: Are you sure? The server is running. If possible, please do a manual shutdown of the target server first (attach-to-server to stop). This command FORCE stops the server and would make server data CORRUPT. The server data will NOT be erased. Players may experience a downtime at most 5 minutes. [(y)es/(N)o/(a)ttach-to-server] "
elif [ "$(docker ps -aq -f name=$PAPER_CONTAINER_NAME)" ]; then
    CONFIRM_DEPLOY_MESSAGE="Deploying PaperSpigot $PAPER_VERSION to $1: Are you sure? The server data will NOT be erased. [y/N] "
else
    CONFIRM_DEPLOY_MESSAGE="Deploying PaperSpigot $PAPER_VERSION to $1: Are you sure? [y/N] "
fi

if ! confirm_deploy "$CONFIRM_DEPLOY_MESSAGE" ; then
    docker update --restart=always $PAPER_CONTAINER_NAME &>/dev/null
    log "Aborting."
    exit 0
fi

# ---

mkdir -p $PAPER_DATA_PATH

# ---

if [ "$(docker ps -aq -f name=$PAPER_CONTAINER_NAME -f status=running)" ]; then
    # running, cleanup

    log "Stopping container $PAPER_CONTAINER_NAME ..."
    docker stop $PAPER_CONTAINER_NAME
    echo
fi

if [ "$(docker ps -aq -f status=exited -f name=$PAPER_CONTAINER_NAME)" ] ||
   [ "$(docker ps -aq -f status=created -f name=$PAPER_CONTAINER_NAME)" ]; then
    # not running, cleanup

    log "Removing container $PAPER_CONTAINER_NAME ..."
    docker rm $PAPER_CONTAINER_NAME
    echo
fi

log "Logging into Docker registry ..."
docker login -u "$MC_DOCKER_REGISTRY_USERNAME" -p "$MC_DOCKER_REGISTRY_PASSWORD" $MC_DOCKER_REGISTRY_HOST
echo

log "Pulling PaperSpigot $PAPER_VERSION files ..."
docker pull $IMAGE_NAME
echo

log "Starting container $PAPER_CONTAINER_NAME ..."
docker run -e LOCAL_USER_ID=`id -u $USER` -e "JAVA_ARGS=$PAPER_JAVA_LAUNCHFLAGS" -itd --network minecraft --name $PAPER_CONTAINER_NAME -v $PAPER_DATA_PATH:/data $PAPER_DOCKER_ADDITIONAL_FLAGS --restart=always $IMAGE_NAME
echo

log "Agreeing EULA ..."
echo "eula=true" > $PAPER_DATA_PATH/eula.txt
echo

log "A new PaperSpigot instance has been deployed."
log "    Attach: \$ docker attach $PAPER_CONTAINER_NAME"
log "    View logs: \$ docker logs -f $PAPER_CONTAINER_NAME"

popd &>/dev/null
