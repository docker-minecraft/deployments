#!/bin/bash

####################################################
#                                                  #
#             Property of Birkhoff Lee             #
#          DO NOT REDISTRIBUTE THIS FILE           #
#                                                  #
#               https://birkhoff.me                #
#                                                  #
####################################################

# ----

bold=$(tput bold)
reset=$(tput sgr0)
red=$(tput setaf 1)
green=$(tput setaf 2)
prefix="$bold== "

log() {
    echo
    echo -e "$prefix$1$reset"
}

confirm() {
    read -r -p "${prefix}$1" response
    case "$response" in
        [yY][eE][sS]|[yY]) 
            true
            ;;
        *)
            false
            ;;
    esac
}

if [ "$EUID" -ne 0 ] ; then
    log "${red}Please run the script as root!"
    exit
fi

read -p "${prefix}Step 1/8 | Timezone (http://goo.gl/dTbRJR): ${reset}" TIMEZONE
read -p "${prefix}Step 2/8 | DDS auth token: ${reset}" DDS_AUTHTOKEN
read -p "${prefix}Step 3/8 | Docker registry user: ${reset}" MC_DOCKER_REGISTRY_USERNAME
read -p "${prefix}Step 4/8 | Docker registry password: ${reset}" MC_DOCKER_REGISTRY_PASSWORD
read -p "${prefix}Step 5/8 | BungeeCord (y/n): ${reset}" DEPLOY_BUNGEE
read -p "${prefix}Step 6/8 | Paper (y/n): ${reset}" DEPLOY_PAPER
read -p "${prefix}Step 7/8 | Spigot (y/n): ${reset}" DEPLOY_SPIGOT
read -p "${prefix}Step 8/8 | MySQL (y/n): ${reset}" DEPLOY_MYSQL

if ! confirm "Confirm (y/N): " ; then
    log "Aborting."
    exit 0
fi

cd

# ----

log "Updating APT list ..."

apt-get update

# ----

log "Installing APT https module ..."

apt-get install -y apt-transport-https

# ----

log "Adding Docker APT repository ..."

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository \
  "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) \
  stable"

# ----

log "Updating APT list ..."

apt-get update

# ----

log "Upgrading system packages ..."

apt-get upgrade -y

# ----

if hash docker 2>/dev/null; then
    log "Removing existing Docker installation ..."
    apt-get remove docker docker-engine
fi

# ----

log "Installing required packages ..."

apt-get install -y \
  linux-image-extra-virtual \
  ca-certificates \
  curl \
  software-properties-common \
  unattended-upgrades \
  fail2ban \
  git \
  ufw \
  vim \
  nano \
  tmux \
  htop \
  wget \
  docker-ce
#   linux-image-extra-$(uname -r)

# ----

log "Securing SSHd ..."

sed -i 's/PermitRootLogin yes/PermitRootLogin no/' /etc/ssh/sshd_config
sed -i 's/#PasswordAuthentication yes/PasswordAuthentication no/' /etc/ssh/sshd_config

# ----

log "Restarting SSHd ..."

service ssh restart

# ----

log "Securing system ..."

echo 'APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Unattended-Upgrade "1";' > /etc/apt/apt.conf.d/20auto-upgrades
echo 'APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Download-Upgradeable-Packages "1";
APT::Periodic::AutocleanInterval "7";
APT::Periodic::Unattended-Upgrade "1";' > /etc/apt/apt.conf.d/10periodic
echo 'Unattended-Upgrade::Allowed-Origins {
    "Ubuntu lucid-security";
    //"Ubuntu lucid-updates";
};' > /etc/apt/apt.conf.d/50unattended-upgrades

# ----

log "Creating Docker network ..."

docker network create minecraft

# ----

log "Creating user \"minecraft\" ..."

useradd -s /bin/bash minecraft
mkdir -p /home/minecraft /home/minecraft/.ssh /usr/local/bin/cfg

# ----

log "Adding SSH key for user \"minecraft\" ..."

echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQClxraaf0yoYrLWhxD1gdFopR/20Z54spf4ecbAXTch42HLpMimFh18qNa4easbeP8SPjt94arV+BxHFmCaK41YsP1tJL/5I1DsnPyYew5ZJjWGBeGF7nMvht5ostKPOa+tpuQP/z5goE7gQxF+46nMnO2q7bHG4+6xl4fLq+yoWY8vpBgW6RUTPPExhHXaV+KxFBmroW1AXvcM2nxnrAQFcG6mvPhnIUpDjYtUvq48lch34MwuX+ckPYXCBinDTekS/ZV9/H4XjVsv/Uay3cAz5VG5SuwXsSCJfgMJFGw86wYRcFe401rnG1LnWJczHPCtzA6CuY25UdptcQYCH+lV birkhoff@Birkhoffs-MBPR.local" > /home/minecraft/.ssh/authorized_keys

# ----

log "Setting up directory permissions of user \"minecraft\" ..."

chmod 700 /home/minecraft/.ssh
chmod 400 /home/minecraft/.ssh/authorized_keys
chown -R minecraft:minecraft /home/minecraft

# ----

log "Adding sudo permission for user \"minecraft\" ..."

groupadd nopwsudo
echo "%nopwsudo ALL=(ALL:ALL) NOPASSWD:ALL" >> /etc/sudoers
usermod -aG nopwsudo minecraft

# ----

log "Adding Docker permission for user \"minecraft\" ..."

usermod -aG docker minecraft

# ----

log "Adding Docker registry login credentials ..."

echo "export MC_DOCKER_REGISTRY_HOST=registry.birkhoff.me" > /home/minecraft/.bashrc
echo "export MC_DOCKER_REGISTRY_USERNAME=$MC_DOCKER_REGISTRY_USERNAME" >> /home/minecraft/.bashrc
echo "export MC_DOCKER_REGISTRY_PASSWORD=$MC_DOCKER_REGISTRY_PASSWORD" >> /home/minecraft/.bashrc

# ----

log "Adding DDS login credentials ..."

echo "export DDS_DOWNLOAD_URL=https://dds.birkhoff.me/download" >> /home/minecraft/.bashrc
echo "export DDS_AUTHTOKEN=$DDS_AUTHTOKEN" >> /home/minecraft/.bashrc

# ----

log "Testing Docker registry login ..."

. /home/minecraft/.bashrc
DOCKER_LOGIN_STATUS=$(docker login -u "$MC_DOCKER_REGISTRY_USERNAME" -p "$MC_DOCKER_REGISTRY_PASSWORD" $MC_DOCKER_REGISTRY_HOST)

if [ "$DOCKER_LOGIN_STATUS" == "Login Succeeded" ] ; then
    log "${green}Successfully logged in to the Docker registry."
else
    log "${red}WARNING: Failed login to the Docker registry! $DOCKER_LOGIN_STATUS"
fi

# ----

log "Applying .bashrc for user \"minecraft\" ..."

echo 'if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
        . "$HOME/.bashrc"
    fi
fi' > /home/minecraft/.profile

# ----

log "Reconfiguring timezone data ... (changing to $TIMEZONE)"

ln -fs /usr/share/zoneinfo/$TIMEZONE /etc/localtime
dpkg-reconfigure -f noninteractive tzdata

# ----

log "Downloading automated deploy scripts ..."

cd /usr/local/bin

if [ "$DEPLOY_BUNGEE" == "y" ] ; then
    curl $DDS_DOWNLOAD_URL/bungee/$DDS_AUTHTOKEN -o deploy_bungee
    mkdir -p cfg/bungee
fi
if [ "$DEPLOY_PAPER" == "y" ] ; then
    curl $DDS_DOWNLOAD_URL/paper/$DDS_AUTHTOKEN -o deploy_paper
    mkdir -p cfg/paper
fi
if [ "$DEPLOY_SPIGOT" == "y" ] ; then
    curl $DDS_DOWNLOAD_URL/spigot/$DDS_AUTHTOKEN -o deploy_spigot
    mkdir -p cfg/spigot
fi
if [ "$DEPLOY_MYSQL" == "y" ] ; then
    curl $DDS_DOWNLOAD_URL/mysql/$DDS_AUTHTOKEN -o deploy_mysql
    mkdir -p cfg/mysql
fi

chmod +x deploy_*

cd

# ----

log "${green}Installation done. Please run \"rm prepare && history -c && history -w\"."
