#!/bin/bash

####################################################
#                                                  #
#             Property of Birkhoff Lee             #
#          DO NOT REDISTRIBUTE THIS FILE           #
#                                                  #
#               https://birkhoff.me                #
#                                                  #
####################################################

# ---

pushd ~ &>/dev/null

bold=$(tput bold)
reset=$(tput sgr0)
prefix="$bold== "

# ---

log() {
    echo $prefix$1$reset
}

confirm_deploy() {
    read -r -p "${prefix}$1${reset}" response
    case "$response" in
        [yY][eE][sS]|[yY]) 
            true
            ;;
        *)
            false
            ;;
    esac
}

# ---

if [ -z "$1" ]; then
    log "Please provide a valid MySQL server to deploy."
    log "For example: deploy_mysql mysql01"
    exit 1
fi

SERVER_CFG_FILE_PATH="/usr/local/bin/cfg/mysql/$1.cfg"
declare -a required_arguments=(
    "MYSQL_CONTAINER_NAME"
    "MYSQL_ROOT_PASSWORD"
    "MYSQL_ROOT_PASSWORD_FILE_LOCATION"
    "MYSQL_DATA_LOCATION"
    "MYSQL_VERSION"
    "MYSQL_DOCKER_IMAGE"
    "MYSQL_LAUNCH_FLAGS")

if [ ! -f "$SERVER_CFG_FILE_PATH" ]; then
    log "Couldn't find that server."
    exit 1
fi

for i in "${required_arguments[@]}"; do
    if ! grep -q "$i=" "$SERVER_CFG_FILE_PATH" ; then
        log "The configuration file of that server is corrupt, please contact me."
        exit 1
    fi
done

. $SERVER_CFG_FILE_PATH

# ---

if [ "$(docker ps -aq -f name=$MYSQL_CONTAINER_NAME -f status=running)" ]; then
    CONFIRM_DEPLOY_MESSAGE="Deploying $MYSQL_DOCKER_IMAGE $MYSQL_VERSION to $1: Are you sure? The $MYSQL_DOCKER_IMAGE server is running. The data will NOT be erased. Players may experience a \"weird\" gameplay for at most 2 minutes. [y/N] "
elif [ "$(docker ps -aq -f name=$MYSQL_CONTAINER_NAME)" ]; then
    CONFIRM_DEPLOY_MESSAGE="Deploying $MYSQL_DOCKER_IMAGE $MYSQL_VERSION to $1: Are you sure? The data will NOT be erased. [y/N] "
else
    CONFIRM_DEPLOY_MESSAGE="Deploying $MYSQL_DOCKER_IMAGE $MYSQL_VERSION to $1: Are you sure? [y/N] "
fi

if ! confirm_deploy "$CONFIRM_DEPLOY_MESSAGE" ; then
    log "Aborting."
    exit 0
fi

# ---

mkdir -p $MYSQL_DATA_LOCATION

# ---

if [ "$(docker ps -aq -f name=$MYSQL_CONTAINER_NAME -f status=running)" ]; then
    # running, cleanup

    log "Stopping container $MYSQL_CONTAINER_NAME ..."
    docker stop $MYSQL_CONTAINER_NAME
    echo
fi

if [ "$(docker ps -aq -f status=exited -f name=$MYSQL_CONTAINER_NAME)" ] ||
   [ "$(docker ps -aq -f status=created -f name=$MYSQL_CONTAINER_NAME)" ]; then
    # not running, cleanup

    log "Removing container $MYSQL_CONTAINER_NAME ..."
    docker rm $MYSQL_CONTAINER_NAME
    echo
fi

log "Writing ROOT password to $MYSQL_ROOT_PASSWORD_FILE_LOCATION ..."
echo "MYSQL_ROOT_PASSWORD=$MYSQL_ROOT_PASSWORD" > $MYSQL_ROOT_PASSWORD_FILE_LOCATION
echo

log "Pulling MySQL $MYSQL_VERSION files ..."
docker pull $MYSQL_DOCKER_IMAGE:$MYSQL_VERSION
echo

log "Starting container $MYSQL_CONTAINER_NAME ..."
docker run -e LOCAL_USER_ID=`id -u $USER` --env-file $MYSQL_ROOT_PASSWORD_FILE_LOCATION -d --name $MYSQL_CONTAINER_NAME --network minecraft -v $MYSQL_DATA_LOCATION:/var/lib/mysql --restart=always $MYSQL_DOCKER_IMAGE:$MYSQL_VERSION $MYSQL_LAUNCH_FLAGS
echo

log "A new MySQL instance has been deployed."
log "    Launch MySQL CLI: \$ docker run -it --net minecraft --rm mysql mysql -h"$MYSQL_CONTAINER_NAME" -uroot -p\"$MYSQL_ROOT_PASSWORD\""
log "    View MySQL logs: \$ docker logs -f $MYSQL_CONTAINER_NAME"

popd &>/dev/null

