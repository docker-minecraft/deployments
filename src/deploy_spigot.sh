#!/bin/bash

####################################################
#                                                  #
#             Property of Birkhoff Lee             #
#          DO NOT REDISTRIBUTE THIS FILE           #
#                                                  #
#               https://birkhoff.me                #
#                                                  #
####################################################

# ---

pushd ~ &>/dev/null

bold=$(tput bold)
reset=$(tput sgr0)
prefix="$bold== "

# ---

log() {
    echo $prefix$1$reset
}

confirm_deploy() {
    read -r -p "${prefix}$1${reset}" response
    case "$response" in
        [yY][eE][sS]|[yY]) 
            true
            ;;
        [aA]) 
            log "Use Ctrl-P Ctrl-Q to escape. DO NOT use Ctrl-C. Use \"stop\" to stop the server. Attaching to $SPIGOT_CONTAINER_NAME ..."
            echo
            docker attach $SPIGOT_CONTAINER_NAME
            echo
            log "Exiting ... (run again to deploy the server)"
            exit 0
            ;;
        *)
            false
            ;;
    esac
}

# ---

if [ -z "$1" ]; then
    log "Please provide a valid server to deploy."
    log "For example: deploy_spigot skywars1"
    exit 1
fi

SERVER_CFG_FILE_PATH="/usr/local/bin/cfg/spigot/$1.cfg"
declare -a required_arguments=(
    "SPIGOT_CONTAINER_NAME"
    "SPIGOT_JAVA_LAUNCHFLAGS"
    "SPIGOT_DATA_PATH"
    "SPIGOT_DOCKER_ADDITIONAL_FLAGS"
    "SPIGOT_VERSION")

if [ ! -f "$SERVER_CFG_FILE_PATH" ]; then
    log "Couldn't find that server."
    exit 1
fi

for i in "${required_arguments[@]}"; do
    if ! grep -q "$i=" "$SERVER_CFG_FILE_PATH" ; then
        log "The configuration file of that server is corrupt, please contact me."
        exit 1
    fi
done

. $SERVER_CFG_FILE_PATH

IMAGE_NAME=$MC_DOCKER_REGISTRY_HOST/minecraft/spigot:$SPIGOT_VERSION

# ---

if [ "$(docker ps -aq -f name=$SPIGOT_CONTAINER_NAME -f status=running)" ]; then
    docker update --restart=no $SPIGOT_CONTAINER_NAME &>/dev/null
    CONFIRM_DEPLOY_MESSAGE="Deploying Spigot $PAPER_VERSION to $1: Are you sure? The server is running. If possible, please do a manual shutdown of the target server first (attach-to-server to stop). This command FORCE stops the server and would make server data CORRUPT. The server data will NOT be erased. Players may experience a downtime at most 5 minutes. [(y)es/(N)o/(a)ttach-to-server] "
elif [ "$(docker ps -aq -f name=$SPIGOT_CONTAINER_NAME)" ]; then
    CONFIRM_DEPLOY_MESSAGE="Deploying Spigot $PAPER_VERSION to $1: Are you sure? The server data will NOT be erased. [y/N] "
else
    CONFIRM_DEPLOY_MESSAGE="Deploying Spigot $PAPER_VERSION to $1: Are you sure? [y/N] "
fi

if ! confirm_deploy "$CONFIRM_DEPLOY_MESSAGE" ; then
    docker update --restart=always $SPIGOT_CONTAINER_NAME &>/dev/null
    log "Aborting."
    exit 0
fi

# ---

mkdir -p $SPIGOT_DATA_PATH

# ---

if [ "$(docker ps -aq -f name=$SPIGOT_CONTAINER_NAME -f status=running)" ]; then
    # running, cleanup

    log "Stopping container $SPIGOT_CONTAINER_NAME ..."
    docker stop $SPIGOT_CONTAINER_NAME
    echo
fi

if [ "$(docker ps -aq -f status=exited -f name=$SPIGOT_CONTAINER_NAME)" ] ||
   [ "$(docker ps -aq -f status=created -f name=$SPIGOT_CONTAINER_NAME)" ]; then
    # not running, cleanup

    log "Removing container $SPIGOT_CONTAINER_NAME ..."
    docker rm $SPIGOT_CONTAINER_NAME
    echo
fi

log "Pulling Spigot $SPIGOT_VERSION files ..."
docker pull $IMAGE_NAME
echo

log "Starting container $SPIGOT_CONTAINER_NAME ..."
docker run -e LOCAL_USER_ID=`id -u $USER` -e "JAVA_ARGS=$SPIGOT_JAVA_LAUNCHFLAGS" -itd --network minecraft --name $SPIGOT_CONTAINER_NAME -v $SPIGOT_DATA_PATH:/data $SPIGOT_DOCKER_ADDITIONAL_FLAGS --restart=always $IMAGE_NAME
echo

log "Agreeing EULA ..."
echo "eula=true" > $SPIGOT_DATA_PATH/eula.txt
echo

log "A new Spigot instance has been deployed."
log "    Attach: \$ docker attach $SPIGOT_CONTAINER_NAME"
log "    View logs: \$ docker logs -f $SPIGOT_CONTAINER_NAME"

popd &>/dev/null
