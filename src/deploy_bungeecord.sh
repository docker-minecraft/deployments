#!/bin/bash

####################################################
#                                                  #
#             Property of Birkhoff Lee             #
#          DO NOT REDISTRIBUTE THIS FILE           #
#                                                  #
#               https://birkhoff.me                #
#                                                  #
####################################################

# ---

pushd ~ &>/dev/null

bold=$(tput bold)
normal=$(tput sgr0)
prefix="$bold== "

# ---

log() {
    echo $prefix$1$normal
}

confirm_deploy() {
    read -r -p "${prefix}$1${reset}" response
    case "$response" in
        [yY][eE][sS]|[yY]) 
            true
            ;;
        [aA]) 
            log "Use Ctrl-P Ctrl-Q to escape. DO NOT use Ctrl-C. Use \"end\" to stop the server. Attaching to $BUNGEECORD_CONTAINER_NAME ..."
            echo
            docker attach $BUNGEECORD_CONTAINER_NAME
            echo
            log "Exiting ... (run again to deploy the server)"
            exit 0
            ;;
        *)
            false
            ;;
    esac
}

# ---

if [ -z "$1" ]; then
    log "Please provide a valid server to deploy."
    log "For example: deploy_bungee bungee01"
    exit 1
fi

SERVER_CFG_FILE_PATH="/usr/local/bin/cfg/bungee/$1.cfg"
declare -a required_arguments=(
    "BUNGEECORD_CONTAINER_NAME"
    "BUNGEECORD_JAVA_LAUNCHFLAGS"
    "BUNGEECORD_DATA_PATH"
    "BUNGEECORD_DOCKER_ADDITIONAL_FLAGS")

if [ ! -f "$SERVER_CFG_FILE_PATH" ]; then
    log "Couldn't find that server."
    exit 1
fi

for i in "${required_arguments[@]}"; do
    if ! grep -q "$i=" "$SERVER_CFG_FILE_PATH" ; then
        log "The configuration file of that server is corrupt, please contact me."
        exit 1
    fi
done

. $SERVER_CFG_FILE_PATH

# ---

if [ "$(docker ps -aq -f status=running -f name=$BUNGEECORD_CONTAINER_NAME)" ]; then
    docker update --restart=no $BUNGEECORD_CONTAINER_NAME &>/dev/null
    CONFIRM_DEPLOY_MESSAGE="Deploying server $1: Are you sure? The server is running. If possible, please do a manual shutdown of the target server first (attach-to-server to stop). This command FORCE stops the server and would make server data CORRUPT. The server data will NOT be erased. Players may experience a downtime at most 5 minutes. [(y)es/(N)o/(a)ttach-to-server] "
elif [ "$(docker ps -aq -f name=$BUNGEECORD_CONTAINER_NAME)" ]; then
    CONFIRM_DEPLOY_MESSAGE="Deploying server $1: Are you sure? The server data will NOT be erased. [y/N] "
else
    CONFIRM_DEPLOY_MESSAGE="Deploying server $1: Are you sure? [y/N] "
fi

if ! confirm_deploy "$CONFIRM_DEPLOY_MESSAGE" ; then
    docker update --restart=always $BUNGEECORD_CONTAINER_NAME &>/dev/null
    log "Aborting."
    exit 0
fi

# ---

mkdir -p $BUNGEECORD_DATA_PATH

IMAGE_NAME=$MC_DOCKER_REGISTRY_HOST/minecraft/bungeecord

# ---

if [ "$(docker ps -aq -f status=running -f name=$BUNGEECORD_CONTAINER_NAME)" ]; then
    # running, cleanup

    log "Stopping container $BUNGEECORD_CONTAINER_NAME ..."
    docker stop $BUNGEECORD_CONTAINER_NAME
    echo
fi

if [ "$(docker ps -aq -f status=exited -f name=$BUNGEECORD_CONTAINER_NAME)" ] ||
   [ "$(docker ps -aq -f status=created -f name=$BUNGEECORD_CONTAINER_NAME)" ]; then
    # not running, cleanup

    log "Removing container $BUNGEECORD_CONTAINER_NAME ..."
    docker rm $BUNGEECORD_CONTAINER_NAME
    echo
fi

log "Logging into Docker registry ..."
docker login -u "$MC_DOCKER_REGISTRY_USERNAME" -p "$MC_DOCKER_REGISTRY_PASSWORD" $MC_DOCKER_REGISTRY_HOST
echo

log "Pulling BungeeCord files ..."
docker pull $IMAGE_NAME
echo

log "Starting container $BUNGEECORD_CONTAINER_NAME ..."
docker run -e LOCAL_USER_ID=`id -u $USER` -e "JAVA_ARGS=$BUNGEECORD_JAVA_LAUNCHFLAGS" -itd --network minecraft --name $BUNGEECORD_CONTAINER_NAME -v $BUNGEECORD_DATA_PATH:/data $BUNGEECORD_DOCKER_ADDITIONAL_FLAGS --restart=always $IMAGE_NAME
echo

log "A new BungeeCord instance has been deployed."
log "${bold}   Attach: \$ docker attach $BUNGEECORD_CONTAINER_NAME"
log "${bold}   View logs: \$ docker logs -f $BUNGEECORD_CONTAINER_NAME"

popd &>/dev/null
